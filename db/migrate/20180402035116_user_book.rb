class UserBook < ActiveRecord::Migration[5.1]
  def change
  	create_table :user_books do |t|
  		t.integer :user_id
  		t.integer :book_id
  		t.integer :reading_status, default: 0
  		t.boolean :is_favorite
  	end
  end
end
