class Comment < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.integer :book_id
      t.string :content
      t.datetime :datetime
    end
  end
end
