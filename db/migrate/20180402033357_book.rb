class Book < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.string :image
      t.integer :page
      t.datetime :publish_date
      t.integer :category_id
      t.string :description
      t.float :rating_score

      t.timestamps null: false

    end
  end
end
