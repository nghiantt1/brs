class RequestBook < ActiveRecord::Migration[5.1]
  def change
    create_table :request_books do |t|
      t.integer :user_id
      t.string :title
      t.string :author
      t.integer :status, default: 0
    end
  end
end
