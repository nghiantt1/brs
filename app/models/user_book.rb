class UserBook < ApplicationRecord
  belongs_to :user
  belongs_to :book

  enum :reading_status [:unread, :reading, :readed]
end
