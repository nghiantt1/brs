class RequestBook < ApplicationRecord
  belongs_to :user

  enum :status [:pending, :approve, :reject]
end
