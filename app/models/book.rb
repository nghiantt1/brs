class Book < ApplicationRecord
  has_many :user_books
  has_many :comments
  belongs_to :category
end
